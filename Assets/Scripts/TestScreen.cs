﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScreen : MonoBehaviour
{
    public Text MessageTitles;

    // Start is called before the first frame update
    void Start()
    {
        WebSocketController WSC = new WebSocketController();
        WSC.InitiateWebSocketConnection(false);

        WSC.OnMessage += OnNewMessageArrived;
    }

    public void OnNewMessageArrived(object sender, OnMessageEventArgs e)
    {
        Debug.Log("---- FROM SCREEN ----");
        Debug.Log(e.Message);
        MessageTitles.text = e.Message.title;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
