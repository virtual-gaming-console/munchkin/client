﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestController : MonoBehaviour
{
    public Text MessageTitles;
    public Button ConnectButton;
    // Start is called before the first frame update
    void Start()
    {
        ConnectButton.onClick.AddListener(delegate { Connect(); });
    }

    public void Connect()
    {
        WebSocketController WSC = new WebSocketController();
        WSC.InitiateWebSocketConnection(true);

        WSC.OnMessage += OnNewMessageArrived;
    }

    public void OnNewMessageArrived(object sender, OnMessageEventArgs e)
    {
        Debug.Log("---- FROM CONTROLLER ----");
        Debug.Log(e.Message);
        MessageTitles.text = e.Message.title;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
