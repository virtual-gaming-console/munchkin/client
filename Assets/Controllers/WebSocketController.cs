using System;
using NativeWebSocket;
using System.Net.Sockets;
using UnityEngine.Networking;
using System.Text;
using UnityEngine;

public class WebSocketController
{
    private string ConnectionString = "ws://localhost:5555";
    // public static string connectionString = "server.munckin.playdeviant.com";
    public string LobbyCode = "123456";
    public string PlayerId = "5f1174a68fd0290079c2b574";

    private WebSocket WebSocket;

    private bool asController;

    public event EventHandler<OnMessageEventArgs> OnMessage;

    protected virtual void OnNewMessage(OnMessageEventArgs e)
    {
        EventHandler<OnMessageEventArgs> handler = OnMessage;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    public async void InitiateWebSocketConnection(bool asController)
    {
        this.WebSocket = new WebSocket($"{ConnectionString}");

        this.WebSocket.OnOpen += () =>
        {
            if (!asController)
            {
                this.Send(new WsMessage() { title = "init-game", data = LobbyCode });
            }
            else
            {
                this.Send(new WsMessage() { title = "join-game", data = LobbyCode, playerId = PlayerId });
            }
        };

        this.WebSocket.OnMessage += (byte[] msg) =>
        {
            WsMessage mess = JsonUtility.FromJson<WsMessage>(Encoding.UTF8.GetString(msg, 0, msg.Length));
            OnNewMessage(new OnMessageEventArgs() { Message = mess });
        };

        this.WebSocket.OnError += (e) =>
        {
            Debug.Log("WS error: " + e);
        };

        this.WebSocket.OnClose += (e) =>
        {
            Debug.Log("WS closed");
        };

        await this.WebSocket.Connect();
    }

    public void Send(WsMessage message)
    {
        string json = JsonUtility.ToJson(message);

        this.WebSocket.Send(Encoding.UTF8.GetBytes(json));
    }
}