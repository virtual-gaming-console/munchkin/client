using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

[Serializable]
public class WsMessage
{
    // for exemple add-card
    public string title;
    public string data;
    public string playerId;

    public override string ToString()
    {
        return $"[{title}] {data}";
    }
}